// Programa para funciones del botton
const para = document.querySelector('button');

      para.addEventListener('click', updateName);

      function updateName() {
        let name = prompt('Ingresa el nombre del jugador');
        para.textContent = 'Jugador 1: ' + name;
      }
      
      //Esto es una clase que representa la logica del juego por lo que es el modelo
export default class Game {
    //definimos un constructor
    constructor() {
        //este es el inicio del juego en que letra va a iniciar el juego
        this.turn = "X";
        //esto representa al tablero
        this.board = new Array(9).fill(null);
    }

    //este elemento lo que hace es cambiar el turno de x a 0
    nextTurn() {
        this.turn = this.turn === "X" ? "O" : "X";
    }

    //esto es un condicional para decir que si hay una casilla ocupada no la sobre  escriba
    makeMove(i) {
        //con esta condicional el juego sabe si está en progreso o no
        if (!this.isInProgress()) {
            return;
        }

        if (this.board[i]) {
            return;
        }

        this.board[i] = this.turn;

        if (!this.findWinningCombination()) {
            this.nextTurn();
        }
    }

    //esto es una matriz para los resultados ganadores en estas posiciones del array
    //por eso le coloque busca la combinacion ganadora
    findWinningCombination() {
        const winningCombinations = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6]
        ];

        //recorremos nuestra matriz y verificamos que los resultados sean iguales a la tabla de resultados
        for (const combination of winningCombinations) {
            const [a, b, c] = combination;

            if (this.board[a] && (this.board[a] === this.board[b] && this.board[a] === this.board[c])) {
                return combination;
            }
        }

        return null;
    }

    //si no hay una combinacion ganadora sigue en ejecucion el juego hasta que llena todas las casilla
    //se combierte en un empate si todo está lleno
    isInProgress() {
        return !this.findWinningCombination() && this.board.includes(null);
    }
}