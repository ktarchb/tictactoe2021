//esta es la vista general de juego
export default class GameView {
    constructor(root) {
        //esta raiz es para mantener una referencia a ese elemento
        this.root = root;
        //con este codigo creamos un encabezado para nuestro juego y el boton de reefresacar
        this.root.innerHTML = `
            <div class="header">
                <div class="header__turn"></div>
                <div class="header__status"></div>
                <button type="button" class="header__restart">
                    <i class="material-icons">refresh</i>
                </button>
            </div>
            <div class="board">
                <div class="board__tile" data-index="0"></div>
                <div class="board__tile" data-index="1"></div>
                <div class="board__tile" data-index="2"></div>
                <div class="board__tile" data-index="3"></div>
                <div class="board__tile" data-index="4"></div>
                <div class="board__tile" data-index="5"></div>
                <div class="board__tile" data-index="6"></div>
                <div class="board__tile" data-index="7"></div>
                <div class="board__tile" data-index="8"></div>
            </div>
        `;

        //estas funciones son para refrescar el juego
        this.onTileClick = undefined;
        this.onRestartClick = undefined;

        //esta arrow function sirve para el llenado de las casillas
        //llama al mosaico al hacer click sobre el
        this.root.querySelectorAll(".board__tile").forEach(tile => {
            tile.addEventListener("click", () => {
                if (this.onTileClick) {
                    this.onTileClick(tile.dataset.index);
                }
            });
        });

        //esta funcion es para reiniciar el juego
        this.root.querySelector(".header__restart").addEventListener("click", () => {
            if (this.onRestartClick) {
                this.onRestartClick();
            }
        });
    }

    update(game) {
        this.updateTurn(game);
        this.updateStatus(game);
        this.updateBoard(game);
    }

    updateTurn(game) {
        this.root.querySelector(".header__turn").textContent = `${game.turn} es tu turno`;
    }

    updateStatus(game) {
        let status = "En progreso";

        if (game.findWinningCombination()) {
            status = `${game.turn} es el GANADOR!`;
        } else if (!game.isInProgress()) {
            status = "Es un empate!";
        }

        this.root.querySelector(".header__status").textContent = status;
    }

    updateBoard(game) {
        const winningCombination = game.findWinningCombination();

        for (let i = 0; i < game.board.length; i++) {
            const tile = this.root.querySelector(`.board__tile[data-index="${i}"]`);

            tile.classList.remove("board__tile--winner");
            tile.textContent = game.board[i];

            if (winningCombination && winningCombination.includes(i)) {
                tile.classList.add("board__tile--winner");
            }
        }
    }
}